/* import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}

} */

/* import { Component, OnInit } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { EmployeeService } from '../employee.service';
import { EmployeePage } from '../employee/employee.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {
  public employees : [] = [];
  constructor(public modalCtrl: ModalController, public employeeService : EmployeeService) {


  }
    ngOnInit() {

            this.employeeService.createPouchDB();

            this.employeeService.read()
            .then(employees => {
                    this.employees = employees;
                })
                .catch((err)=>{});

    }


    showDetails(employee) {
        let modal = this.modalCtrl.create(EmployeePage, { employee: employee });
        modal.present();
    }  


}

 */

import { Component, NgZone, OnInit  } from '@angular/core';
import { ModalController, NavController, Platform  } from '@ionic/angular';
//import { EmployeePage } from './../employee/employee.page.ts';
import { EmployeeService } from '../employee.service';
import { EmployeePage } from '../employee/employee.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  public employees : [] = [];
  constructor(
    private modalCtrl: ModalController, 
    private platform: Platform,
    private zone: NgZone,
    private employeeService : EmployeeService
    ) {

    }

    ionViewDidLoad() {
      this.platform.ready().then(() => {
          this.employeeService.createPouchDB();

         /*  this.employeeService.read()
              .then(data => {
                  this.zone.run(() => {
                      this.employees = data;
                  });
              })
              .catch(console.error.bind(console)); */
      });
  }

    /* showDetails(employee) {
        let modal = this.modalCtrl.create(EmployeePage, { employee: employee });
        modal.present();
    } */
}


